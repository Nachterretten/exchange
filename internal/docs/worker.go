package docs

import "exchange/internal/infastacrute/modules/worker/controller"

//go:generate swagger generate spec -o ../../static/swagger.json --scan-models

// swagger:route GET /history worker workerHistoryRequest
// Хранение истории цен криптовалюты.
// responses:
//   200: workerHistoryResponse

// swagger:response workerHistoryResponse
//
//nolint:all
type workerHistoryResponse struct {
	// in:body
	Body []controller.ProfileResponseHistory
}

// swagger:route GET /max worker workerMaxRequest
// Получение списка пар криптовалют с максимальной ценой.
// responses:
//   200: workerMaxResponse

// swagger:response workerMaxResponse
//
//nolint:all
type workerMaxResponse struct {
	// in:body
	Body []controller.ProfileResponseMax
}

// swagger:route GET /min worker workerMinRequest
// Получение списка пар криптовалют с минимальной ценой.
// responses:
//   200: workerMinResponse

// swagger:response workerMinResponse
//
//nolint:all
type workerMinResponse struct {
	// in:body
	Body []controller.ProfileResponseMin
}

// swagger:route GET /avg worker workerAvgRequest
// Вывод списка пар криптовалют со средней ценой.
// responses:
//   200: workerAvgResponse

// swagger:response workerAvgResponse
//
//nolint:all
type workerAvgResponse struct {
	// in:body
	Body []controller.ProfileResponseAvg
}
