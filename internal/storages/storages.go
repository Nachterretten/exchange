package storages

import (
	"exchange/internal/db/adapter"
	"exchange/internal/infastacrute/modules/worker/storage"
)

type Storages struct {
	Worker storage.Workerer
}

func NewStorage(sqlAdapter *adapter.SQLAdapter) *Storages {
	return &Storages{Worker: storage.NewWorkerStorage(sqlAdapter)}
}
