package rabbit

import (
	"encoding/json"
	model "exchange/internal/models/worker"
	amqp "github.com/rabbitmq/amqp091-go"
	"golang.org/x/net/context"
	"log"
)

func RabbitStart(dto model.SellDto) {
	conn, err := amqp.Dial("amqp://guest:guest@rabbitmq:5672/")
	if err != nil {
		log.Fatal("Failed to connect to RabbitMQ", err)
	}

	ch, err := conn.Channel()
	if err != nil {
		log.Fatal("Failed to open a channel", err)
	}

	q, err := ch.QueueDeclare(
		"adder", // name
		true,    // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)

	body, _ := json.Marshal(dto)
	err = ch.PublishWithContext(
		context.Background(),
		"",
		q.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        body,
		})
	if err != nil {
		log.Fatal("Failed to publish a message", err)
	}
	log.Println(" [x] Sent", dto.SellPrice)
}
