package worker

import (
	"exchange/internal/infastacrute/components"
	"exchange/internal/infastacrute/modules/worker/service"
	"exchange/internal/storages"
)

type Services struct {
	Worker service.Workerer
}

func NewServices(storages *storages.Storages, components *components.Components) *Services {
	return &Services{
		Worker: service.NewWorkerService(storages.Worker, components.Logger, components),
	}
}
