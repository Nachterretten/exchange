package worker

import (
	"exchange/internal/infastacrute/components"
	"exchange/internal/infastacrute/modules/worker/controller"
)

type Controllers struct {
	Worker controller.Workerer
}

func NewControllers(services *Services, components *components.Components) *Controllers {

	workerController := controller.NewWorker(services.Worker, components)

	return &Controllers{
		Worker: workerController,
	}
}
