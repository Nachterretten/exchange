package storage

import (
	"context"
	model "exchange/internal/models/worker"
)

type Workerer interface {
	TickerWork(ctx context.Context, model model.WorkerDTO) (int, error)
	MaxMinAvg(ctx context.Context, model model.WorkerMaxMinAvgoDto) (int, error)
	MinPrice(ctx context.Context) ([]model.WorkerMinPrice, error)
	MaxPrice(ctx context.Context) ([]model.WorkerMaxPrice, error)
	AvgPrice(ctx context.Context) ([]model.WorkerAvgPrice, error)
	History(ctx context.Context) ([]model.WorkerDTO, error)
	Sell(ctx context.Context, dto model.SellDto) error
}
