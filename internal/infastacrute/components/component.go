package components

import (
	"exchange/config"
	"exchange/internal/infastacrute/responder"
	"github.com/ptflp/godecoder"
	"go.uber.org/zap"
)

type Components struct {
	Conf config.AppConf
	//Notify       service.Notifier
	//TokenManager cryptography.TokenManager
	Responder responder.Responder
	Decoder   godecoder.Decoder
	Logger    *zap.Logger
	//Hash         cryptography.Hasher
}

func NewComponents(conf config.AppConf, responder responder.Responder, decoder godecoder.Decoder, logger *zap.Logger) *Components {
	return &Components{Conf: conf, Responder: responder, Decoder: decoder, Logger: logger}
}
