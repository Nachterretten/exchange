package router

import (
	"exchange/internal/infastacrute/components"
	modules "exchange/internal/infastacrute/modules/worker"
	"github.com/go-chi/chi/v5"
	"net/http"
)

func NewRouter(controllers *modules.Controllers, components *components.Components) *chi.Mux {
	r := chi.NewRouter()
	setDefaultRoutes(r)
	worker := controllers.Worker
	r.Route("/", func(r chi.Router) {
		r.Get("/min", worker.MinPrices)
		r.Get("/max", worker.MaxPrices)
		r.Get("/avg", worker.AveragePrices)
		r.Get("/history", worker.History)
	})
	return r
}

func setDefaultRoutes(r *chi.Mux) {
	r.Get("/swagger", swaggerUI)
	r.Get("/static/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))).ServeHTTP(w, r)
	})
}
