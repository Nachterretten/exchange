package worker

import (
	"encoding/json"
	"exchange/internal/infastacrute/modules/worker/service"
	model "exchange/internal/models/worker"
	"go.uber.org/zap"
	"net/http"
	"time"
)

type Worker interface {
	WorkerRun()
}

type Works struct {
	logger  *zap.Logger
	service service.Workerer
}

func (w *Works) WorkerRun() {
	ticker := time.NewTicker(10 * time.Second)
	w.logger.Info("getting cryptocurrency prices")

	go func() {
		for {
			select {
			case <-ticker.C:
				cryptoWorkerBody, err := http.Get("https://api.exmo.com/v1.1/ticker")
				if err != nil {
					w.logger.Error("http.Get", zap.Error(err))
				}
				body := map[string]model.WorkerDTO{}
				err = json.NewDecoder(cryptoWorkerBody.Body).Decode(&body)
				if err != nil {
					w.logger.Error("jsonDecoder", zap.Error(err))
				}
				MaxMinAvg := model.WorkerMaxMinAvgoDto{}
				SellPrice := model.SellDto{}
				for pair, value := range body {
					value.Name = pair
					// история цен
					w.service.TickerWork(value)
					// максимальная, мин, авг цены
					MaxMinAvg.Name = value.Name
					MaxMinAvg.High = value.High
					MaxMinAvg.Low = value.Low
					MaxMinAvg.Avg = value.Avg
					w.service.MaxMinAvg(MaxMinAvg)
					SellPrice.Name = value.Name
					SellPrice.SellPrice = value.SellPrice
					w.service.SellDto(SellPrice)
				}
			}
		}
	}()
}

func NewWorkerRun(logger *zap.Logger, service service.Workerer) Worker {
	return &Works{logger: logger, service: service}
}
