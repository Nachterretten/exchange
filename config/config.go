package config

import (
	"go.uber.org/zap"
	"os"
	"strconv"
	"time"
)

const (
	AppName = "APP_NAME"

	serverPort         = "SERVER_PORT"
	envShutdownTimeout = "SHUTDOWN_TIMEOUT"

	parseShutdownTimeoutError    = "config: parse server shutdown timeout error"
	parseRpcShutdownTimeoutError = "config: parse rpc server shutdown timeout error"
)

//go:generate easytags $GOFILE yaml, json

type AppConf struct {
	AppName     string    `yaml:"app_name" json:"app_name"`
	Environment string    `yaml:"environment" json:"environment"`
	Domain      string    `yaml:"domain" json:"domain"`
	APIUrl      string    `yaml:"api_url" json:"api_url"`
	Server      Server    `yaml:"server" json:"server"`
	Cors        Cors      `yaml:"cors" json:"cors"`
	Token       Token     `yaml:"token" json:"token"`
	Provider    Provider  `yaml:"provider" json:"provider"`
	Logger      Logger    `yaml:"logger" json:"logger"`
	DB          DB        `yaml:"db" json:"db"`
	Cache       Cache     `yaml:"cache" json:"cache"`
	RPCServer   RPCServer `yaml:"rpc_server" json:"rpc_server"`
	UserRPC     RPCClient `yaml:"client_rpc" json:"user_rpc"`
	AuthRPC     RPCClient `yaml:"client_rpc" json:"auth_rpc"`
	WorkerRPC   RPCClient `yaml:"worker_rpc" json:"worker_rpc"`
}

type RPCClient struct {
	Host string `yaml:"host" json:"host"`
	Port string `yaml:"port" json:"port"`
}

type DB struct {
	Net      string `yaml:"net" json:"net"`
	Driver   string `yaml:"driver" json:"driver"`
	Name     string `yaml:"name" json:"name"`
	User     string `yaml:"user" json:"user"`
	Password string `yaml:"password" json:"password"`
	Host     string `yaml:"host" json:"host"`
	MaxConn  int    `yaml:"max_conn" json:"max_conn"`
	Port     string `yaml:"port" json:"port"`
	Timeout  int    `yaml:"timeout" json:"timeout"`
}

type RPCServer struct {
	Port         string        `yaml:"port" json:"port"`
	ShutdownTime time.Duration `yaml:"shutdown_time" json:"shutdown_time"`
	Type         string        `yaml:"type" json:"type"`
	Host         string        `yaml:"host" json:"host"`
}

type Cache struct {
	Address  string `yaml:"address" json:"address"`
	Password string `yaml:"password" json:"password"`
	Port     string `yaml:"port" json:"port"`
}

type Logger struct {
	Level string `yaml:"level" json:"level"`
}

type Email struct {
	VerifyLinkTTL time.Duration `yaml:"verify_link_ttl" json:"verify_link_ttl"`
	From          string        `yaml:"from" json:"from"`
	Port          string        `yaml:"port" json:"port"`
	Credentials   Credentials   `yaml:"credentials" json:"credentials"`
}

type Provider struct {
	Email Email `yaml:"email" json:"email"`
	Phone Phone `yaml:"phone" json:"phone"`
}

type Phone struct {
	VerifyCodeTTl time.Duration `yaml:"verify_code_t_tl" json:"verify_code_t_tl"`
	Credentials   Credentials   `yaml:"credentials" json:"credentials"`
}

type Credentials struct {
	Host        string `yaml:"host" json:"host"`
	Login       string `yaml:"login" json:"login"`
	Password    string `yaml:"password" json:"password"`
	AccessToken string `yaml:"access_token" json:"access_token"`
	Secret      string `yaml:"secret" json:"secret"`
	Key         string `yaml:"key" json:"key"`
	FilePath    string `yaml:"file_path" json:"file_path"`
}

type Token struct {
	AccessTTL     time.Duration `yaml:"access_ttl" json:"access_ttl"`
	RefreshTTL    time.Duration `yaml:"refresh_ttl" json:"refresh_ttl"`
	AccessSecret  string        `yaml:"access_secret" json:"access_secret"`
	RefreshSecret string        `yaml:"refresh_secret" json:"refresh_secret"`
}

type Cors struct {
	// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
	AllowedOrigins   []string `yaml:"allowed_origins" json:"allowed_origins"`
	AllowedMethods   []string `yaml:"allowed_methods" json:"allowed_methods"`
	AllowedHeaders   []string `yaml:"allowed_headers" json:"allowed_headers"`
	ExposedHeaders   []string `yaml:"exposed_headers" json:"exposed_headers"`
	AllowCredentials bool     `yaml:"allow_credentials" json:"allow_credentials"`
	MaxAge           int      `yaml:"max_age" json:"max_age"`
}

type Server struct {
	Port            string        `yaml:"port" json:"port"`
	ShutdownTimeout time.Duration `yaml:"shutdown_timeout" json:"shutdown_timeout"`
}

func newCors() *Cors {
	return &Cors{
		// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"https://*", "http://*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}
}

func NewAppConf() AppConf {
	port := os.Getenv(serverPort)

	return AppConf{
		AppName: os.Getenv(AppName),
		Server: Server{
			Port: port,
		},
		DB: DB{
			Net:      os.Getenv("DB_NET"),
			Driver:   os.Getenv("DB_DRIVER"),
			Name:     os.Getenv("DB_NAME"),
			User:     os.Getenv("DB_USER"),
			Password: os.Getenv("DB_PASSWORD"),
			Host:     os.Getenv("DB_HOST"),
			Port:     os.Getenv("DB_PORT"),
		},
		Cache: Cache{
			Address:  os.Getenv("CACHE_ADDRESS"),
			Password: os.Getenv("CACHE_PASSWORD"),
			Port:     os.Getenv("CACHE_PORT"),
		},
		Cors: *newCors(),
	}
}

func (a *AppConf) Init(logger *zap.Logger) {
	shutDownTimeSeconds, err := strconv.Atoi(os.Getenv(envShutdownTimeout))
	if err != nil {
		logger.Fatal(parseRpcShutdownTimeoutError)
	}
	shutDownTimeOut := time.Duration(shutDownTimeSeconds) * time.Second
	if err != nil {
		logger.Fatal(parseRpcShutdownTimeoutError)
	}

	dbTimeout, err := strconv.Atoi(os.Getenv("DB_TIMEOUT"))
	if err != nil {
		logger.Fatal("config: parse db timeout err", zap.Error(err))
	}
	dbMaxConn, err := strconv.Atoi(os.Getenv("MAX_CONN"))
	if err != nil {
		logger.Fatal("config: parse db max conn err", zap.Error(err))
	}
	a.DB.Timeout = dbTimeout
	a.DB.MaxConn = dbMaxConn

	a.Provider.Email.From = os.Getenv("EMAIL_FROM")
	a.Provider.Email.Port = os.Getenv("EMAIL_PORT")
	a.Provider.Email.Credentials.Host = os.Getenv("EMAIL_HOST")
	a.Provider.Email.Credentials.Login = os.Getenv("EMAIL_LOGIN")
	a.Provider.Email.Credentials.Password = os.Getenv("EMAIL_PASSWORD")
	a.Token.AccessSecret = os.Getenv("ACCESS_SECRET")
	a.Token.RefreshSecret = os.Getenv("REFRESH_SECRET")
	a.Domain = os.Getenv("DOMAIN")
	a.APIUrl = os.Getenv("API_URL")

	a.RPCServer.Port = os.Getenv("RPC_PORT")
	a.WorkerRPC.Port = os.Getenv("WORKER_RPC_PORT")
	a.WorkerRPC.Host = os.Getenv("WORKER_RPC_HOST")
	a.RPCServer.Type = os.Getenv("RPC_TYPE")

	a.Server.ShutdownTimeout = shutDownTimeOut
}
