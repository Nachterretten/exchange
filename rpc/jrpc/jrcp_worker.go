package jrpc

import (
	"context"
	"exchange/internal/infastacrute/modules/worker/service"
)

type WorkerServerJRPC struct {
	workerRPC service.Workerer
}

type Empty struct {
}

func NewWorkerServerJRPC(workerJRPC service.Workerer) *WorkerServerJRPC {
	return &WorkerServerJRPC{workerRPC: workerJRPC}
}

func (w *WorkerServerJRPC) History(in Empty, out *service.WorkerHistory) error {
	*out = w.workerRPC.History(context.Background())
	return nil
}

func (w *WorkerServerJRPC) MaxPrice(in Empty, out *service.WorkerMaxPriceOut) error {
	*out = w.workerRPC.MaxPrices(context.Background())
	return nil
}

func (w *WorkerServerJRPC) MinPrice(in Empty, out *service.WorkerMinPriceOut) error {
	*out = w.workerRPC.MinPrices(context.Background())
	return nil
}

func (w *WorkerServerJRPC) AvgPrice(in Empty, out *service.WorkerAvgPriceOut) error {
	*out = w.workerRPC.AvgPrices(context.Background())
	return nil
}
