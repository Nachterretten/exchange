package main

import (
	config2 "exchange/config"
	"exchange/internal/infastacrute/logs"
	"exchange/run"
	"github.com/joho/godotenv"
	"os"
)

func main() {
	err := godotenv.Load()
	config := config2.NewAppConf()
	logger := logs.NewLogger(config, os.Stdout)
	if err != nil {
		logger.Fatal("error loading .env file")
	}
	config.Init(logger)
	app := run.NewApp(config, logger)
	exitCode := app.Bootstrap().Run()
	os.Exit(exitCode)
}
